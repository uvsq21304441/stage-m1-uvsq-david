#coding: utf-8

class lecture():
    def __init__(self,path):
        self.data=[]

        #taille du plateau
        self.lignes=0
        self.colonnes=0

        #Proba de rester sur place par defaut
        self.probaR=0
        #Taille d'une case
        self.unit = 0
        #Coords des cases but et debut
        self.debx = 0
        self.deby = 0
        self.butx = 0
        self.buty = 0

        #liste des cases morts
        self.mort = []
        
        #Liste des murs
        self.murs = []
        
        #Liste des cases ayant une probabilité de deviation non uniforme
        self.casesP = []

        #Nombre de murs que min peut poser
        self.nb_murs_min=0

        self.path = path
        self.init_data()
        self.init_unit()
        self.init_taille()
        self.init_proba()
        self.init_objectifs()
        self.init_mort()
        self.init_murs()
        self.init_probas()
        self.init_murs_min()
        
    def find_index(self, obj):
        try:
            i=0
            while self.data[i]!=obj:
                i+=1
            i+=1
            return i
        except IndexError:
            print("Erreur nom index",obj)
            exit()

    def init_murs_min(self):
        try:
            i=self.find_index("#nb murs min:")
            if(len(self.data)>i):
                self.nb_murs_min=int(self.data[i])
        except ValueError:
            print("Erreur murs min")
            exit()
        
    def init_unit(self):
        i = self.find_index("#taille case:")
        self.unit = int(self.data[i])
        
    def init_data(self):
        #Lit le fichier regles pour permettre la création du plateau
        try:
            fic = open(self.path).readlines()
            #Liste comprenant toute les données du fichier
            self.data=[]

            for elt in fic:
                """Suppression des retours à la ligne
                analyse des données du fichier et création du plateau de jeu """
                self.data.append(elt.replace("\n",""))
                if elt=="\n":
                    self.data.remove("")
        except IOError:
            print("Fichier non existant")
            exit()
            
    def init_taille(self):
        try:
            taille = self.data[1].split("*")
            self.lignes=int(taille[0])
            self.colonnes=int(taille[1])
        except ValueError:
            print("Erreur taille")
            exit()

    def init_proba(self):
        #Proba de rester sur place
        try:
            self.probaR =(float(self.data[8].split(":")[1]))
        except ValueError:
            print("Erreur proba rester sur place")
            exit()
        
    def init_objectifs(self):
        try:
            coordDeb = self.data[3].split(" ")
            coordBut = self.data[5].split(" ")
            self.debx = int(coordDeb[0])
            self.deby = int(coordDeb[1])
            self.butx = int(coordBut[0])
            self.buty = int(coordBut[1])
        except ValueError:
            print("Erreur objectif")
            exit()
            
    def init_mort(self):
        #recuperation des coords des 0-sinks
        try:
            i=self.find_index("#position 0-sink (Mort):")
            while(self.data[i]!="#position murs:"):
                coord = self.data[i].split(" ")
                tupleCoord=(int(coord[0]),int(coord[1]))
                self.mort.append(tupleCoord)
                i+=1
                
        except ValueError:
            print("Erreur placement mort")
            exit()
                
    def init_murs(self):
    #récupération des coords des murs
        try:
            i = self.find_index("#position murs:")
            while(self.data[i]!="#nb murs min:"):
                mur = self.data[i].split(" ")
                tupleMur = ((int(mur[0]),int(mur[1])),(int(mur[2]),int(mur[3])))
                self.murs.append(tupleMur)
                i+=1
        except ValueError:
            print("Erreur placement murs")
            exit()
                
    def init_probas(self):
    #Liste des cases avec une proba de deviation differente
        try:
            i = self.find_index("#modification proba case par case (2 2 r:0.8 h:1 b:2 g:2 d:6)")
            while(self.data[i]!="#position 0-sink (Mort):"):
                data_case = self.data[i].split(" ")
                data_case[0] = int(data_case[0])
                data_case[1] = int(data_case[1])
                if(data_case[2].split(":")[0]=="r"):
                    data_case[2]= (data_case[2].split(":")[0],float(data_case[2].split(":")[1])) 
                else :
                    data_case[2]= (data_case[2].split(":")[0],int(data_case[2].split(":")[1]))
                if len(data_case)>3:
                    for j in range(3,len(data_case)):
                        data_j=data_case[j].split(":")
                        data_case[j]=(data_j[0],int(data_j[1]))
                self.casesP.append(data_case)
                i+=1
	    print self.casesP
        except ValueError:
            print("Erreur proba")
            exit()
            
