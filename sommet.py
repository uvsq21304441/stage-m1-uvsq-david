#coding: utf-8
class Sommet():
    def __init__(self,x,y,Type,pos):
        #Coord
        self.x,self.y=x,y

        #Placement dans la case (g/d/h/b)
        self.pos=pos

        #
        self.probaR=0.9

        #dictionnaire des proba de deviation
        self.probas={"h":1,"b":1,"g":1,"d":1}
        self.voisins={}
        self.valeur=0
        self.Type=Type

    #Fct qui permet de changer les probas de deplacement aleatoire si les probas d'une case ont été modifiées dans le fichier .txt
    def set_AVE(self,x,y,liste_cases):
        for elt in liste_cases:
            if x==elt[0] and y==elt[1]:
                for i in range(2,len(elt)):
                    if elt[i][0]=="r":
                        self.probaR=elt[i][1]
                    else:
                        self.probas[elt[i][0]]=elt[i][1]
        
        
    def aff(self):
        print(self.x,self.y,self.Type,self.pos)
            
    def aff_voi(self):
        for elt in self.voisins.values():
            elt.aff()
