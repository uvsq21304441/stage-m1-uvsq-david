#coding: utf-8
import cProfile
import collections
import random as random
import numpy as np
from numpy import linalg as la
from itertools import combinations
#import sys
from sommet import *
from strat_max import *
from strat_min import *
import copy

class Grille_modele():
    """La grille est de taile nbligne*nbcolonne, mort represente la liste des cases mort
    Murs represente la liste comprenant les positions des murs
    Deviation represente la probabilité de rester sur la case aprés déplacement
    lpc represente la liste des cases  ayant un comportement non uniforme
    Observateur permet de notifier la création d'element graphique à la vue associé au modele
    """
    def __init__(self,plateau,observateur):
        self.test=[0]*2
        #Variable contenant toutes les informations du plateau
        self.plateau = plateau
        #taille plateau
        self.nblignes = self.plateau.lignes
        self.nbcolonnes = self.plateau.colonnes
        self.observateur = observateur        
        #dictionnaire referancant les cases murs/morts

        self.nb_min=self.plateau.nb_murs_min
        self.dico_combi={}
        self.combi_murs={}

        self.tmp_combi_mur=None

        self.dead=False
        self.win=False
        self.pion=(self.plateau.debx,self.plateau.deby)
        self.observateur.dessine_pion(self.pion)

        self.aff_mur(self.plateau.murs)
        self.init_combi()
        self.init_jeux_terminaux(None,False)

        for k,v in self.combi_murs.items():
            if len(k)==1:
                print k
                
    def initialisation_graphe(self):
        self.init_sommets()
        self.sup_case_voi_but()
        self.init_voi_but()
        self.sup_link_mur()
        self.init_murs()
        self.init_link()

    def initialisation_strat_min(self):
        #creation des strategies
        s2=Strategie_min(self.observateur)
        s2.init_strat(self.sommets)
        return s2
    
    def initialisation_strat_max(self):
        s=Strategie_max(self.observateur)
        s.strategie_parcours_largeur(self.nbcolonnes,self.nblignes,self.plateau.mort,self.plateau.murs,self.plateau.butx,self.plateau.buty)
        return s

    def initialisation_murs(self,murs):
        self.sommets_max={}
        self.sommets={}
        self.dico_index={}
        self.index_to_sommets={}
        self.tmp_combi_mur=murs
        murs=self.list_index_to_list_murs(murs)
        self.plateau.murs=list(self.cp_murs)
        return murs

    """
    Si i vaut le nombre maximum de murs que le joueur Min 
    peut poser alors le jeu en construction est une feuille 
    et le nombre de mur restant est 0
    """
    def set_nb_murs_min_restant(self,strat_min,nb_murs_pose):
        if nb_murs_pose!=self.nb_min:
            strat_min.nb_murs=self.nb_min-nb_murs_pose
        else:
            strat_min.nb_murs=0

    def initialisation_dictionnaire_valeurs(self,murs,strat_max):
        dic_val_case={}
        for mur in murs:
	    if mur[0][0]==self.plateau.butx and mur[0][1]==self.plateau.buty:
	        dic_val_case[mur[0]]=strat_max.valeur[mur[0][0],mur[0][1],"but"]
	    else:
	        dic_val_case[mur[0]]=strat_max.valeur[mur[0][0],mur[0][1],"max"]
	    if mur[1][0]==self.plateau.butx and mur[1][1]==self.plateau.buty:
	        dic_val_case[mur[1]]=strat_max.valeur[mur[1][0],mur[1][1],"but"]
	    else:
	        dic_val_case[mur[1]]=strat_max.valeur[mur[1][0],mur[1][1],"max"]
        self.combi_murs[self.list_murs_to_list_index(murs)]=dic_val_case
    """
    La fonction permet de trouver le jeu optimal du jeu définit par le fichier texte.
    La résolution se fait à partir des feuilles et remonte le graphe des sous-jeu.
    Toute les valeurs sont gardées en mémoire lorsque l'on remonte dans le graphe.
    """
    def init_jeux_terminaux(self,info,clicked):
        self.combi_murs={}
        if clicked:
            self.plateau.murs.append(info)
            self.nb_min-=1
            self.init_combi()
        self.cp_murs=list(self.plateau.murs)
        #Boucle sur nb_murs, qui represente le nombre de murs déjà posé par min.
        #On commence par résoudre les jeux où tout les murs de min sont posés.(nb_murs=nb_min)
        nb_murs_pose=self.nb_min
        while nb_murs_pose>=0:
            for murs in self.dico_combi[nb_murs_pose]:
                murs=self.initialisation_murs(murs)
                if self.verif_combi_mur(murs):
                    self.plateau.murs+=list(murs)
                    self.initialisation_graphe()
                    s=self.initialisation_strat_max()
                    s2=self.initialisation_strat_min()
                    self.set_nb_murs_min_restant(s2,nb_murs_pose)
                    self.strat_init(s,s2)
                    self.initialisation_dictionnaire_valeurs(murs,s)	      
                else:
                    self.combi_murs[self.list_murs_to_list_index(murs)]="impossible"
            nb_murs_pose-=1
        self.s_max=s
        self.s_min=s2
        self.affiche_jeu(s,s2)

    def affiche_jeu(self,strat_max,strat_min):
        self.init_obj()
        self.init_mort()
        self.aff_mur(self.plateau.murs)
        strat_max.affiche_arrow()
        strat_max.affiche_res()
        strat_min.affiche_murs()
        
    def est_case_mort(self,case):
        return case in self.plateau.mort

    def est_case_but(self,pion):
        if pion[0]==self.plateau.butx and pion[1]==self.plateau.buty:
            return True
        return False
    
    """
    Tire au sort un voisin du sommet som selon les poids des voisins.
    Retourne la position du voisin choisit aléatoirement.
    """
    def choix_som_rand(self,som):
        #On recupere la somme des poids des voisins
        poids_tot=sum([poids_som for poids_som in som.probas.values()])
        tab_val=4*[0]
        tab_som=4*[0]
        cpt=0
        #Indexation des poids des voisins et de leur position relative au sommet som.
        for voisin,proba in som.probas.items():
            tab_val[cpt]=proba
            tab_som[cpt]=voisin
            cpt+=1
        sum_poids=0
        #On tire au hasard un entier entre 1 et le poids total
        r=random.randint(1,poids_tot)
        for i in range(cpt):
            sum_poids+=tab_val[i]
            if r<=sum_poids:
                return (self.find_ind_max(som.x,som.y,tab_som[i]),tab_som[i])
            
    """
    Fonction qui permet de faire une simulation d'une partie.
    """
    def simulation(self):
        proba = self.sommets[self.pion[0],self.pion[1],"max"].probaR
        r = random.uniform(0.0,1.0)
        #Tirage aléatoire pour savoir si déplacement aléatoire ou non
        if r > proba:
            print "deplacement aléatoire : "
            res = self.choix_som_rand(self.sommets[self.pion[0],self.pion[1],"AVE"])
            pion2=res[0]
            dep=res[1]
            print " dep : ", dep, " coords : ",pion2
        else:
            print "Joueur max suit sa strategie"
            #Recuperation des coordonnees du sommet donné par la strategie du joueur max.
            dep = self.s_max.deplacement[self.pion]
            pion2 = self.find_ind_max(self.pion[0],self.pion[1],dep)


        if self.est_case_mort(pion2):
            print "Dead"
            self.observateur.dessine_pion(pion2)
            self.dead=True
            
        if self.est_case_but(pion2):
            print "Win"
            self.observateur.dessine_pion(pion2)
            self.win=True
            
        if self.dead==False and self.win==False:
            #Calcul du nouveau jeu optimal du nouveau sous jeu
            if self.s_min.action[self.pion[0],self.pion[1],"min",dep]==1:
                print " Joueur min bloque"
                self.sup_res()
                self.observateur.rm_mur_min()
                self.init_jeux_terminaux((self.pion,pion2),True)
                self.observateur.dessine_pion(self.pion)
            else:  
                #Permet d'afficher la nouvelle position du pion dans l'interface graphique
                self.observateur.dessine_pion(pion2)
                self.pion=pion2
        
        
    ###################################### CONSTRUCTION GRAPHE ######################################
    """Pour chaque case on va creer 4 sommet ave,min et sink, 1 sommet AVE
    Si la case et une mort ou le but alors on ne creer qu'un sommet But ou mort
    """
    def init_sommets(self):
        for x in range(self.nblignes):
            for y in range(self.nbcolonnes):
                if x ==self.plateau.butx and y==self.plateau.buty:
                    self.sommets[x,y,"but"]=Sommet(x,y,"but",None)
                    continue
                if (x,y) in self.plateau.mort:
                    self.sommets[(x,y,"mort" )]=Sommet(x,y,"mort",None)
                else:
                    self.sommets_max[x,y]=Sommet(x,y,"max",None)
                    self.sommets[x,y,"max"]=Sommet(x,y,"max",None)
                    
                    self.sommets[(x,y,"min","g")]=Sommet(x,y,"min","g")
                    self.sommets[(x,y,"min","d")]=Sommet(x,y,"min","d")
                    self.sommets[(x,y,"min","h")]=Sommet(x,y,"min","h")
                    self.sommets[(x,y,"min","b")]=Sommet(x,y,"min","b")
                    
                    #On change les probas de base si on a decrit un comportement different pour la case dans le fichier .txt
                    s_AVE=Sommet(x,y,"AVE",None)
                    if(self.case_proba_diff(x,y)):
                        s_AVE.set_AVE(x,y,self.plateau.casesP)
                    self.sommets[(x,y,"AVE")]=s_AVE
                    
                    self.sommets[(x,y,"ave","g")]=Sommet(x,y,"ave","g")
                    self.sommets[(x,y,"ave","d")]=Sommet(x,y,"ave","d")
                    self.sommets[(x,y,"ave","h")]=Sommet(x,y,"ave","h")
                    self.sommets[(x,y,"ave","b")]=Sommet(x,y,"ave","b")
                    
                    self.sommets[(x,y,"sink","g")]=Sommet(x,y,"sink","g")
                    self.sommets[(x,y,"sink","d")]=Sommet(x,y,"sink","d")
                    self.sommets[(x,y,"sink","h")]=Sommet(x,y,"sink","h")
                    self.sommets[(x,y,"sink","b")]=Sommet(x,y,"sink","b")
    """
    Fct qui renseigne les ave menant à la case but et qui les stocke dans les voisins de but afin de pouvoir faire des parcours sur le graphe 
    """
    def init_voi_but(self):
        butx,buty=self.plateau.butx,self.plateau.buty
        if (butx,buty-1,"max") in self.sommets:
            self.sommets[butx,buty,"but"].voisins[butx,buty-1,"ave","d"]=self.sommets[butx,buty-1,"ave","d"]
        if (butx,buty+1,"max") in self.sommets:
            self.sommets[butx,buty,"but"].voisins[butx,buty-1,"ave","g"]=self.sommets[butx,buty-1,"ave","g"]
        if (butx-1,buty,"max") in self.sommets:
            self.sommets[butx,buty,"but"].voisins[butx,buty-1,"ave","b"]=self.sommets[butx,buty-1,"ave","b"]
        if (butx+1,buty,"max") in self.sommets:
            self.sommets[butx,buty,"but"].voisins[butx,buty-1,"ave","h"]=self.sommets[butx,buty-1,"ave","h"]
                    
    """On supprime les sommets qui deviennent inutile à cause des murs
    Pour chaque murs on supprime 6 sommets, 3 pour chaque case touché par le mur (1 ave,1 min,1 sink)
    """
    def sup_link_mur(self):
        for elt in self.plateau.murs:
            lig1,col1=elt[0][0],elt[0][1]
            lig2,col2=elt[1][0],elt[1][1]

            #s1 voisin de gauche de s2
            if lig1==lig2 and col1==col2-1:
                
                if (lig1,col1,"min","d") in self.sommets:
                    del self.sommets[(lig1,col1,"ave","d")]
                    del self.sommets[(lig1,col1,"min","d")]
                    del self.sommets[(lig1,col1,"sink","d")]
                elif (lig1,col1,"ave","d") in self.sommets:
                    del self.sommets[(lig1,col1,"ave","d")]

                if (lig2,col2,"min","g") in self.sommets:
                    del self.sommets[(lig2,col2,"ave","g")]
                    del self.sommets[(lig2,col2,"min","g")]
                    del self.sommets[(lig2,col2,"sink","g")]
                elif (lig2,col2,"ave","g") in self.sommets:
                    del self.sommets[(lig1,col1,"ave","g")]
                    
            #s1 voisin de droite de s2
            if lig1==lig2 and col1==col2+1:

                if (lig1,col1,"min","g") in self.sommets:
                    del self.sommets[(lig1,col1,"ave","g")]
                    del self.sommets[(lig1,col1,"min","g")]
                    del self.sommets[(lig1,col1,"sink","g")]
                elif (lig1,col1,"ave","g") in self.sommets:
                    del self.sommets[(lig1,col1,"ave","g")]

                if (lig2,col2,"min","d") in self.sommets:
                    del self.sommets[(lig2,col2,"ave","d")]
                    del self.sommets[(lig2,col2,"min","d")]
                    del self.sommets[(lig2,col2,"sink","d")]
                elif (lig2,col2,"ave","d") in self.sommets:
                    del self.sommets[(lig1,col1,"ave","d")]
                
            #s1 voisin haut de s2
            if lig1==lig2-1 and col1==col2:
                if (lig1,col1,"min","b") in self.sommets:
                    del self.sommets[(lig1,col1,"ave","b")]
                    del self.sommets[(lig1,col1,"min","b")]
                    del self.sommets[(lig1,col1,"sink","b")]
                elif (lig1,col1,"ave","b") in self.sommets:
                    del self.sommets[(lig1,col1,"ave","b")]

                if (lig2,col2,"min","h") in self.sommets:
                    del self.sommets[(lig2,col2,"ave","h")]
                    del self.sommets[(lig2,col2,"min","h")]
                    del self.sommets[(lig2,col2,"sink","h")]
                elif (lig2,col2,"ave","h") in self.sommets:
                    del self.sommets[(lig2,col2,"ave","h")]
                
            #s1 voisin bas de s2
            if lig1==lig2+1 and col1==col2:

                if (lig1,col1,"min","h") in self.sommets:
                    del self.sommets[(lig1,col1,"ave","h")]
                    del self.sommets[(lig1,col1,"min","h")]
                    del self.sommets[(lig1,col1,"sink","h")]
                elif (lig1,col1,"ave","h") in self.sommets:
                    del self.sommets[(lig1,col1,"ave","h")]

                if (lig2,col2,"min","b") in self.sommets:
                    del self.sommets[(lig2,col2,"ave","b")]
                    del self.sommets[(lig2,col2,"min","b")]
                    del self.sommets[(lig2,col2,"sink","b")]
                elif (lig2,col2,"ave","b") in self.sommets:
                    del self.sommets[(lig1,col1,"ave","b")]

    """On supprime les sommets des cases se trouvant aux extremités du plateau
    Par exemple les cases sur la premiere colonne du plateau n'ont pas de voisins à 
    gauche on va donc supprimer les sommets correspondant (1 min,1 sink,1 ave)
    """
    def init_murs(self):
        for elt in self.sommets_max.values():
            #cote haut
            if elt.x==0:
                del self.sommets[(elt.x,elt.y,"ave","h")]
                del self.sommets[(elt.x,elt.y,"min","h")]
                del self.sommets[(elt.x,elt.y,"sink","h")]
            #cote bas
            if elt.x==self.nblignes-1:
                del self.sommets[(elt.x,elt.y,"ave","b")]
                del self.sommets[(elt.x,elt.y,"min","b")]
                del self.sommets[(elt.x,elt.y,"sink","b")]
            #cote gauche
            if elt.y==0:
                del self.sommets[(elt.x,elt.y,"ave","g")]
                del self.sommets[(elt.x,elt.y,"min","g")]
                del self.sommets[(elt.x,elt.y,"sink","g")]
            #cote droit
            if elt.y==self.nbcolonnes-1:
                del self.sommets[(elt.x,elt.y,"ave","d")]
                del self.sommets[(elt.x,elt.y,"min","d")]
                del self.sommets[(elt.x,elt.y,"sink","d")]

    """
    On supprime les sommets min et sink d'une direction pour chaque sommets max ayant la case but pour voisin
    Puis set les voisins de ave et AVE de la direction vers le but
    """
    def sup_case_voi_but(self):
        
        butx,buty=self.plateau.butx,self.plateau.buty

        if (butx,buty-1,"max") in self.sommets:
            del self.sommets[(butx,buty-1,"min","d")]
            del self.sommets[(butx,buty-1,"sink","d")]
            self.sommets[(butx,buty-1,"ave","d")].voisins[butx,buty,"but"]=self.sommets[(butx,buty,"but")]
            self.sommets[(butx,buty-1,"AVE")].voisins[butx,buty,"but"]=self.sommets[(butx,buty,"but")]

        if (butx,buty+1,"max") in self.sommets:
            del self.sommets[(butx,buty+1,"min","g")]
            del self.sommets[(butx,buty+1,"sink","g")]
            self.sommets[(butx,buty+1,"ave","g")].voisins[butx,buty,"but"]=self.sommets[(butx,buty,"but")]
            self.sommets[(butx,buty+1,"AVE")].voisins[butx,buty,"but"]=self.sommets[(butx,buty,"but")]

        if (butx+1,buty,"max") in self.sommets:
            del self.sommets[(butx+1,buty,"min","h")]
            del self.sommets[(butx+1,buty,"sink","h")]
            self.sommets[(butx+1,buty,"ave","h")].voisins[butx,buty,"but"]=self.sommets[(butx,buty,"but")]
            self.sommets[(butx+1,buty,"AVE")].voisins[butx,buty,"but"]=self.sommets[(butx,buty,"but")]

        if (butx-1,buty,"max") in self.sommets:
            del self.sommets[(butx-1,buty,"min","b")]
            del self.sommets[(butx-1,buty,"sink","b")]
            self.sommets[(butx-1,buty,"ave","b")].voisins[butx,buty,"but"]=self.sommets[(butx,buty,"but")]
            self.sommets[(butx-1,buty,"AVE")].voisins[butx,buty,"but"]=self.sommets[(butx,buty,"but")]


    """Fonction qui permet de creer les liaisons entre chaque sommets. 
    """
    def init_link(self):
        for key,s in self.sommets.items():
            if key[2]=="max":
                if (s.x,s.y,"ave","g") in self.sommets and self.find_ind_max(s.x,s.y,"g") not in self.plateau.mort:
                    s.voisins[(s.x,s.y,"ave","g")]=self.sommets[(s.x,s.y,"ave","g")]
                    
                if (s.x,s.y,"ave","d") in self.sommets and self.find_ind_max(s.x,s.y,"d") not in self.plateau.mort:
                    s.voisins[(s.x,s.y,"ave","d")]=self.sommets[(s.x,s.y,"ave","d")]
                    
                if (s.x,s.y,"ave","h") in self.sommets and self.find_ind_max(s.x,s.y,"h") not in self.plateau.mort:
                    s.voisins[(s.x,s.y,"ave","h")]=self.sommets[(s.x,s.y,"ave","h")]
                    
                if (s.x,s.y,"ave","b") in self.sommets and self.find_ind_max(s.x,s.y,"b") not in self.plateau.mort:
                    s.voisins[(s.x,s.y,"ave","b")]=self.sommets[(s.x,s.y,"ave","b")]
            
            if key[2]=="ave":
                s.voisins[(s.x,s.y,"AVE")]=self.sommets[(s.x,s.y,"AVE")]
                if (s.x,s.y,"min",key[3]) in self.sommets:
                    s.voisins[(s.x,s.y,"min",key[3])]=self.sommets[(s.x,s.y,"min",key[3])]
                
            if key[2]=="min":
                coord=self.find_ind_max(s.x,s.y,key[3])
                
                if (coord[0],coord[1],"max") in self.sommets:
                    s.voisins[(s.x,s.y,"sink",key[3])]=self.sommets[(s.x,s.y,"sink",key[3])]
                    s.voisins[(coord[0],coord[1],"max")]=self.sommets[(coord[0],coord[1],"max")]
                    
                elif (coord[0],coord[1],"mort") in self.sommets:
                    del self.sommets[(s.x,s.y,"min",key[3])]
                    del self.sommets[(s.x,s.y,"sink",key[3])]
                    del self.sommets[(s.x,s.y,"ave",key[3])]
                    self.sommets[(s.x,s.y,"AVE")].voisins[coord[0],coord[1],"mort"]=self.sommets[(coord[0],coord[1],"mort")]
                    if key in self.sommets[(s.x,s.y,"AVE")].voisins:
                        del self.sommets[(s.x,s.y,"AVE")].voisins[key]
                #link to sink and max
            if key[2]=="AVE":
                if (s.x,s.y,"min","g") in self.sommets:
                    s.voisins[(s.x,s.y,"min","g")]=self.sommets[(s.x,s.y,"min","g")]
                elif (s.x,s.y-1,"mort")not in self.sommets and (s.x,s.y-1,"but")not in self.sommets:
                    del s.probas["g"]
                    
                if (s.x,s.y,"min","d") in self.sommets:
                    s.voisins[(s.x,s.y,"min","d")]=self.sommets[(s.x,s.y,"min","d")]
                elif (s.x,s.y+1,"mort")not in self.sommets and (s.x,s.y+1,"but")not in self.sommets:
                    del s.probas["d"]
                    
                if (s.x,s.y,"min","h") in self.sommets:
                    s.voisins[(s.x,s.y,"min","h")]=self.sommets[(s.x,s.y,"min","h")]
                elif (s.x-1,s.y,"mort")not in self.sommets and (s.x-1,s.y,"but")not in self.sommets:
                    del s.probas["h"]
                    
                if (s.x,s.y,"min","b") in self.sommets:
                    s.voisins[(s.x,s.y,"min","b")]=self.sommets[(s.x,s.y,"min","b")]
                elif (s.x+1,s.y,"mort")not in self.sommets and (s.x+1,s.y,"but")not in self.sommets :
                    del s.probas["b"]

    ###################################### Fct utilitaires ######################################

    #les tableaux d'index permette de savoir à quelle position se trouve un sommet dans la matrice
    def maj_index_mat(self):
        i=0
        self.dico_index={}
        self.index_to_sommets={}
        for elt in self.sommets:
            self.dico_index[elt]=i
            self.index_to_sommets[i]=elt
            i+=1

    #Retourne l'index de l'element passer en parametre
    def find_index(self,s):
        return self.dico_index[s]

    """La fonction prends en parametre les coordonnées de deux cases adjancentes representant un mur.
    Elle renvoie le numerant correspondant au mur.
    """
    def murs_to_index(self,x,y,x2,y2):
        if x!=x2:
            if x >x2:
                x=x2
                y=y2
            i=x*((self.nbcolonnes*2)-1)+y+(self.nbcolonnes-1)
        else:
            if y > y2:
                x=x2
                y=y2
            i=x*((self.nbcolonnes*2)-1)+y
        return i
    
    """
    Prends en parametre un indice correspondant à la position d'un mur, et renvoie les coordonnées
    de deux cases adjacentes qui sont séparées par ce mur.
    """
    def index_to_mur(self,i):
        x=int(i/((self.nbcolonnes*2)-1))
        pos=i-x*((self.nbcolonnes*2)-1)
        #mur horizontal
        if pos>=self.nbcolonnes-1:
            y=pos-(self.nbcolonnes-1)
            x2=x+1
            y2=y
        #mur vertical
        else:
            y=pos
            x2=x
            y2=y+1
        return ((x,y),(x2,y2))
    
    """
    Prends en argument une liste de murs et renvoie une liste d'index de ces murs
    """
    def list_murs_to_list_index(self,list_murs):
        res=list()
        for elt in list_murs:
            res.append(self.murs_to_index(elt[0][0],elt[0][1],elt[1][0],elt[1][1]))
        res.sort()
        return tuple(res)

    """
    Prends en argument une liste d'index de mur et renvoie une liste murs
    """
    def list_index_to_list_murs(self,list_index):
        res=list()
        for elt in list_index:
            res.append(self.index_to_mur(elt))
        return tuple(res)

    """
    Renvoie True si L2 est incluse dans L1, False sinon
    """
    def isInclude(self,L1, L2):
        return set.issubset(set(L1), set(L2))

    """
    La fonction permet de trouver les combinaisons des murs. 
    Ces combinaisons representent les murs des potentiels sous-jeux.
    """
    def init_combi(self):
	res=self.find_pos_mur()
        res2=[]
        for m in res:
            res2.append(self.murs_to_index(m[0][0],m[0][1],m[1][0],m[1][1]))
        res2.sort()
	for i in range(self.nb_min+1):
	    self.dico_combi[i]=list(combinations(res2,i))

            
    """Fonction permettant, à partir d'une coordonnees et d'un deplacement
    de trouver les coordonnees du noeud max lie au dpl (ou mort ou but)
    """
    def find_ind_max(self,x,y,voisin):
        if voisin == "h":
            return (x-1,y)
        elif voisin == "b":
            return (x+1,y)
        elif voisin == "g":
            return(x,y-1)
        elif voisin == "d":
            return(x,y+1)

    #Retourne la pos de s2 par rapport a s
    def find_pos(self,sx,sy,s2x,s2y):
        if sx==s2x and sy==s2y-1:
            return "d"
        
        if sx==s2x and sy==s2y+1:
            return "g"
        
        if sx==s2x-1 and sy==s2y:
            return "b"
        
        if sx==s2x+1 and sy==s2y:
            return "h"

    #Fonction qui permet de savoir si la case de coord x,y a une probabilité differente de celle donnée par defaut
    def case_proba_diff(self,x,y):
        for elt in self.plateau.casesP:
            if x==elt[0] and y==elt[1]:
                return True
        return False
    
    #Return True si les deux cases ont un mur les separants
    def is_wall(self,x,y,x2,y2,murs):
        if ((x,y),(x2,y2)) in murs or ((x2,y2),(x,y)) in murs:
            return True
        return False
    """
    La fonction permet d'obtenir les positions potentiellement jouable par Min.
    Un mur potentiellement jouable est un mur qui n'est pas déjà existant et qui n'est pas 
    adjacent à une case mort.
    """
    def find_pos_mur(self):
        list_pos_mur=[]
        for x in range(self.nblignes):
            for y in range(self.nbcolonnes):
                if (x,y) not in self.plateau.mort:
                    if y+1<self.nbcolonnes and (x,y+1) not in self.plateau.mort:
                        if not self.is_wall(x,y,x,y+1,self.plateau.murs):
                            list_pos_mur.append(((x,y),(x,y+1)))
                    if x+1<self.nblignes and (x+1,y) not in self.plateau.mort:
                        if not self.is_wall(x,y,x+1,y,self.plateau.murs):
                            list_pos_mur.append(((x,y),(x+1,y)))
        return list_pos_mur

    """
    Le parametre list_pos_murs est une liste de murs qui comprends les murs prédéfini et une série 
    de murs que Min joue. Il represente donc les murs d'un sous-jeu.
    La fonction vérifie donc si cette liste de mur ne rends pas le graphe non connexe à l'aide d'un
    parcours.
    Si la liste laisse le graphe connexe elle renvoie un boolean de valeur True, False sinon.
    """
    def verif_combi_mur(self,list_pos_murs):
        nbl=self.nblignes
        nbc=self.nbcolonnes
        d={}
        morts=self.plateau.mort

        
        murs=list(self.plateau.murs)
        murs=murs+list(list_pos_murs)
        
        for x in range(nbl):
            for y in range(nbc):
                if(x,y) not in morts:
                    d[x,y]=0
        
        f=collections.deque()
        f.append((self.plateau.butx,self.plateau.buty))

        
        while(len(f)>0):
            s=f.popleft()
            d[s]=1
            if (s[0]-1,s[1]) in d:
                if d[s[0]-1,s[1]]==0 and d[s[0]-1,s[1]] not in morts and self.is_wall(s[0]-1,s[1],s[0],s[1],murs)==False:
                    f.append((s[0]-1,s[1]))
                    d[s[0]-1,s[1]]=1
                    
            if (s[0]+1,s[1]) in d:
                if d[s[0]+1,s[1]]==0 and d[s[0]+1,s[1]] not in morts and self.is_wall(s[0]+1,s[1],s[0],s[1],murs)==False:
                    f.append((s[0]+1,s[1]))
                    d[s[0]+1,s[1]]=1
                    
            if (s[0],s[1]-1) in d:
                if d[s[0],s[1]-1]==0 and d[s[0],s[1]-1] not in morts and self.is_wall(s[0],s[1]-1,s[0],s[1],murs)==False:
                    f.append((s[0],s[1]-1))
                    d[s[0],s[1]-1]=1
                    
            if (s[0],s[1]+1) in d:
                if d[s[0],s[1]+1]==0 and d[s[0],s[1]+1] not in morts and self.is_wall(s[0],s[1]+1,s[0],s[1],murs)==False:
                    f.append((s[0],s[1]+1))
                    d[s[0],s[1]+1]=1


        b=True
        for x in range(nbl):
            for y in range(nbc):
                if(x,y) not in morts and b==True:
                    if d[x,y]==0:
                        b=False
                        return False
        return True
    

    
    ###################################### AFFICHAGE ######################################
    
    """Affiche chaque sommet en precisant ses coordonnees, son type et sa position par 
    rapport à sa case
    """
    def affiche_liaison(self):
        
        for x in range(self.nblignes):
            for y in range(self.nbcolonnes):
                for s in self.sommets.values():
                    if s.x==x and s.y==y:
                        print(" ")
                        if s.Type=="max":
                            s.aff()
                            print("max voisins : ")
                            s.aff_voi()
                        if s.Type=="min":
                            s.aff()
                            print("min voisins : ")
                            s.aff_voi()
                        if s.Type=="ave":
                            s.aff()
                            print("ave voisins : ")
                            s.aff_voi()
                        if s.Type=="AVE":
                            s.aff()
                            print("AVE voisins : ")
                            s.aff_voi()
                        if s.Type=="sink":
                            s.aff()
                            print("sink voisins : ")
                            s.aff_voi()
                        if s.Type=="mort":
                            s.aff()
                            print("mort voisins : ")
                        if s.Type=="but":
                            s.aff()
                            print("but voisins : ")
                            s.aff_voi()
                        
    """Fonction permettant de notifier à la vue les emplacements des murs
    """
    def aff_mur(self,murs):
        for elt in murs:
            self.observateur.create_mur(elt[0][0],elt[0][1],elt[1][0],elt[1][1])
    """
    Fonction permettant de notifier à la vue les emplacements des cases piéges
    """
    def init_mort(self):
        for elt in self.plateau.mort:
            self.observateur.create_mort(elt[0],elt[1])

    """
    Fonction permettant de notifier à la vue les emplacements de la case but et deb
    """
    def init_obj(self):
        params={"x":self.plateau.debx,"y":self.plateau.deby,"Type":"couleur","arg":"gold"}
        self.observateur.maj(params)
        params={"x":self.plateau.butx,"y":self.plateau.buty,"Type":"couleur","arg":"green"}
        self.observateur.maj(params)

    """
    Fonction qui supprime les fleches representant la strategie Max.
    """
    def sup_arrows(self):
        self.observateur.reset_arrows()

    """
    Fonction qui supprime les valeurs representant les probabilités de réussite associée à une case.
    """
    def sup_texts(self):
        self.observateur.reset_texts()

    """
    Fonction qui supprime tout les éléments des strategies Min, Max, ainsi que les pourcentage de réussite.
    """
    def sup_res(self):
        self.observateur.reset_res()

    #Fct qui affiche les valeurs des cases 
    def affiche_res(self,res):
        i=0
        while i<len(res):
            elt=self.index_to_sommets[i]
            if elt[2]=="max" or elt[2]=="but":
                self.add_text(elt[0],elt[1],round(res[i]*100,3))
            i+=1
    """
    Notifie à la vue l'affichage des pourcentage de réussite
    """
    def add_text(self,x,y,val):
        return self.observateur.ajout_text(x,y,val)
                        
    ###################################### RESOLUTION JEU OPTIMAL ######################################
    """
    Fonction qui à partir de deux stratégies trouve le jeu optimal
    """
    def strat_init(self,strat_max,strat_min):
        self.maj_index_mat()
        self.init_sys(strat_max,strat_min)
        self.find_strat_opt(strat_max,strat_min)

    #Creation et resolution du systeme lié au graphe
    def init_sys(self,strat,strat_min):
        
        vec_ligne=[0]*(len(self.sommets))
        vec_ligne[self.find_index((self.plateau.butx,self.plateau.buty,"but"))]=1
        mat=[0]*(len(self.sommets))

        for key,s in self.sommets.items():
            vecl=[0]*(len(self.sommets))
            
            vecl[self.find_index(key)]=1
            if s.Type=="max":
                dpl=strat.deplacement[s.x,s.y]
                index=self.find_index((s.x,s.y,"ave",dpl))
                vecl[index]=-1
                mat[self.find_index((s.x,s.y,"max"))]=vecl


            #FAIRE GESTION IMPOSSIBLE ICI
            if s.Type=="min":
                
                l=list(self.tmp_combi_mur)
                s_v=self.find_ind_max(s.x,s.y,s.pos)
                l.append(self.murs_to_index(s.x,s.y,s_v[0],s_v[1]))
                l.sort()
                l=tuple(l)

                s_max_coord=self.find_ind_max(s.x,s.y,s.pos)
                
                if len(l)<self.nb_min and self.combi_murs[l]=="impossible" :
		    vecl[self.find_index((s_max_coord[0],s_max_coord[1],"max"))]-=1
                    
                #Si le graphe n'est pas une feuille, si min bloque ==> prend val du sink, sinon prends val du max
                elif strat_min.nb_murs>0 : 
                    if strat_min.action[key]==1:
                        vecl[self.find_index((s.x,s.y,"sink",s.pos))]=-1
                    else:
                        vecl[self.find_index((s_max_coord[0],s_max_coord[1],"max"))]-=1

                #jeu final donc min ne peut pas bloquer donc prends la val de max
                else:
                    vecl[self.find_index((s_max_coord[0],s_max_coord[1],"max"))]-=1
                
                mat[self.find_index((s.x,s.y,"min",s.pos))]=vecl
                
            if s.Type=="ave":
                #deplacement du joueur max suivant sa strat 
                dpl=strat.deplacement[s.x,s.y]

                #Proba de ne pas subir de dpl alea
                pR = self.sommets[(s.x,s.y,"max")].probaR
                
                if (s.x,s.y,"min",s.pos) in s.voisins:
                    index_min=self.find_index((s.x,s.y,"min",s.pos))
                    
                elif (self.plateau.butx,self.plateau.buty,"but") in s.voisins:
                    index_min=self.find_index((self.plateau.butx,self.plateau.buty,"but"))

                index_AVE=self.find_index((s.x,s.y,"AVE"))
                vecl[index_min]-=float(pR)
                vecl[index_AVE]-=(1.0-float(pR))
                    
                mat[self.find_index((s.x,s.y,"ave",s.pos))]=vecl
                    
            if s.Type=="AVE":
                poids_tot=sum([poids_som for poids_som in s.probas.values()])
                for key,s2 in s.voisins.items():
                    if s2.Type=="mort" or s2.Type=="but":
                        pos=self.find_pos(s.x,s.y,s2.x,s2.y)
                        vecl[self.find_index((s2.x,s2.y,s2.Type))]-=(float(s.probas[pos])/float(poids_tot))
                    else:
                        vecl[self.find_index((s2.x,s2.y,"min",s2.pos))]-=(float(s.probas[s2.pos])/float(poids_tot))
                
                mat[self.find_index((s.x,s.y,"AVE"))]=vecl
                
            if s.Type=="sink":
                if strat_min.nb_murs>0: 
                    l=list(self.tmp_combi_mur)
                    s_v=self.find_ind_max(s.x,s.y,s.pos)
                    l.append(self.murs_to_index(s.x,s.y,s_v[0],s_v[1]))
                    l.sort()
                    l=tuple(l)
                    if self.combi_murs[l]!="impossible":
                        v=self.combi_murs[l]
                        vec_ligne[self.find_index((s.x,s.y,"sink",s.pos))]=v[s.x,s.y]
                        
                mat[self.find_index((s.x,s.y,"sink",s.pos))]=vecl
                
            if s.Type =="but":
                mat[self.find_index((s.x,s.y,"but"))]=vecl
                
            if s.Type =="mort":
                mat[self.find_index((s.x,s.y,"mort"))]=vecl

        
        res = self.res_sys(mat,vec_ligne)
        self.maj_val_strat(strat,res)

    """
    Résout le système lié au graphe
    """
    def res_sys(self,mat,vec_ligne):
        a = np.array(mat,float)
        b = np.array(vec_ligne,float)
        return la.solve(a,b)

    ##################################### SWITCH STRATEGIE  ######################################
        
    #Fct qui itere jusqu'a trouvé la solution optimal d'un jeu
    def find_strat_opt(self,strat,strat_min):
        #while self.switch_strat(strat):
        while self.switch_min(strat,strat_min):
            res=self.init_sys(strat,strat_min)
        if strat_min.nb_murs>=0:
            b = self.switch_strat(strat)
            #b = self.switch_min(strat,strat_min)
            if b:
                self.init_sys(strat,strat_min)
                self.find_strat_opt(strat,strat_min)
    """
    Fonction qui switch un ensemble d'action de la strategie Min
    """
    def switch_min(self,strat,strat_min):
        switched=False
        for elt in self.sommets:
            if elt[2]=="min":
                x,y,dpl=elt[0],elt[1],elt[3]
                m_s=self.find_ind_max(x,y,dpl)
		#l'attribut self.tmp_combi_mur contient les murs déjà posé par min
                l=list(self.tmp_combi_mur)
                l.append(self.murs_to_index(x,y,m_s[0],m_s[1]))
                l.sort()
                l=tuple(l)
                if len(l)<=self.nb_min and self.combi_murs[l]!="impossible":
                    if strat_min.action[elt]==0 and round(strat.valeur[(x,y,"sink",dpl)],10)< round(strat.valeur[(m_s[0],m_s[1],"max")],10):
                        strat_min.action[elt]=1
                        switched=True
                        self.test[0]+=1
		    if strat_min.action[elt]==1 and round(strat.valeur[(x,y,"sink",dpl)],10)>= round(strat.valeur[(m_s[0],m_s[1],"max")],10):
		        strat_min.action[elt]=0
		        switched=True
                        self.test[1]+=1
        return switched
            
    #Fct qui realise les switchs de Max
    def switch_strat(self,strat):
        switched=False
        for elt in self.sommets:
            if elt[2]=="max":
                x,y=elt[0],elt[1]
                for voisin in self.sommets[x,y,"max"].voisins:
                    dpl = strat.deplacement[x,y]
                    if round(strat.valeur[(x,y,"ave",dpl)],10) < round(strat.valeur[(x,y,"ave",voisin[3])],10):
                        strat.deplacement[(x,y)]=voisin[3]
                        switched=True
        return switched

    #Fct qui permet de mettre à jour les valeurs trouvé par le systeme et les stocke dans le dictionnaire de la strategie max
    def maj_val_strat(self,strat,res):
        i=0
        while i<len(res):
            elt=self.index_to_sommets[i]
            strat.valeur[elt]=res[i]
            i+=1



