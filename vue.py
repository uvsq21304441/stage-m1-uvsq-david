#coding: utf-8

import Tkinter as tk
from lecture import *
from controlleur import *
from modele import *


"""
Dans l'interface graphique x=ligne et y=colonne, la case en haut a gauche est la (0,0)
"""
class Terrain(tk.Tk):
    def __init__(self,path):
        tk.Tk.__init__(self)

        plateau = lecture(path)

        #taille du plateau
        nbligne = plateau.lignes
        nbcolonne = plateau.colonnes

        unit = plateau.unit

        #taille de la fenetre (20 unité d'une case)
        hauteur = nbligne*unit+5
        largeur = nbcolonne*unit+5

        self.title('SSG')

        controlleur = Controlleur(None,None)
        vue = Fenetre(self,controlleur,nbligne,nbcolonne,hauteur,largeur,unit)
        modele = Grille_modele(plateau,vue.grille)

        controlleur.vue = vue
        controlleur.modele = modele

class Fenetre(tk.Frame):
    def __init__(self,parent,controlleur, lignes, colonnes, hauteur,largeur,unit):
        tk.Frame.__init__(self,parent)
        self.grid()
        self.parent = parent
        self.controlleur=controlleur
        self.grille = Grille(self,lignes,colonnes,hauteur,largeur,unit)
        self.grille.grid(row=0,column=1, sticky= 'E')
        self.focus_set()

        menu = tk.Menu(self.parent)
        menu.add_command(label = "quitter", command=parent.destroy)
        menu.add_command(label = "simulation", command = (lambda :self.action("simul")))
        self.parent.config(menu=menu)

    def action(self,command):
        self.controlleur.button(command)

class Grille(tk.Canvas):
    def __init__(self,parent,lignes,colonnes,hauteur,largeur,unit):
        tk.Canvas.__init__(self,width=largeur,height=hauteur,bg="ivory",confine=True)

        self.pion=None
        self.mur_min={}
        self.arrows=[]
        self.texts=[]
        self.largeur = largeur
        self.hauteur = hauteur
        self.parent = parent
        self.unit = unit

	#Création graphique des cases, et stockage dans le quadrillage
        self.quadrillage = [[self.create_rectangle(i*unit,j*unit,(i+1)*unit,(j+1)*unit,fill="#FFFFFF")for i in range(colonnes)] for j in range(lignes)]

    """
    Change la couleur d'une case.
    """
    def change_couleur_case(self,x,y,couleur):
        #On cherche dans le quadrillage la case à modifier
        self.itemconfig(self.quadrillage[x][y],fill=couleur)

    """
    Change la couleur des cases en noirs si elles ont été définies en tant que case "mort" dans les regles du jeu.
    """
    def create_mort(self,x,y):
        self.change_couleur_case(x,y,"Black")

    """
    Fonction qui permet l'affichage d'un mur selon des coordonnées de cases.
    """
    def create_mur(self,x,y,x2,y2):
        unit = self.unit
        pos_mur=self.find_pos_mur(x,y,x2,y2)

        if pos_mur == "g":
            ob = self.create_line(y*unit,x*unit,y*unit,(x+1)*unit,fill="Black",width=6)

        elif pos_mur == "d":
            ob = self.create_line((y+1)*unit,x*unit,(y+1)*unit,(x+1)*unit,fill="Black",width=6)

        elif pos_mur == "h":
            ob = self.create_line(y*unit,x*unit,(y+1)*unit,x*unit,fill="Black",width=6)

        elif pos_mur == "b":
            ob = self.create_line(y*unit,(x+1)*unit,(y+1)*unit,(x+1)*unit,fill="Black",width=6)

    """
    La fonction est appelée suite à un clic de l'utilisateur.
    Elle permet la creation d'un mur suite à un clique sur un mur faisant parti de la strategie de min.
    Elle permet donc de relancer un calcul de jeu optimal avec le meme plateau mais comprenant un mur en plus.
    """
    def onObjectClick(self,event):
        #Recuperation de l'objet cliquer.
        ob = self.find_closest(event.x, event.y)
        #Recuperation des coordonnées et de la position relative du mur.
        ob1 = self.mur_min[ob[0]]
        #Permet de recuperer les coordonnees de la case de l'autre cote du mur
        ob2 = self.find_cases_mur(ob1[0],ob1[1],ob1[2])
        ob1=(ob1[0],ob1[1])
        info =(ob1,ob2)
        #Suppression des murs min restant de l'interface graphique pour calcul de nouvelle strategie min
        self.rm_mur_min()
        #Suppression des resultats precedent afin de permettre l'affichage du futur sous-jeu
        self.reset_res()
        #Permet le calcul du jeu optimal du sous-jeu créé
        self.parent.controlleur.click_mur_min(info)

    """
    Cette fonction permet de trouver la position relative d'un mur (h,b,g,d) à une case, à partir des coordonnées des cases concernées par le mur.
    """
    def find_pos_mur(self,x1,y1,x2,y2):
        if x1==x2+1 and y1==y2:
            return "h"
        elif x1==x2-1 and y1==y2:
            return "b"
        elif y1==y2+1 and x2==x1:
            return "g"
        elif y1==y2-1 and x1==x2:
            return "d"
        else:
            print("Erreur format fichier")
            exit()

    """
    Cette fonction permet, à partir d'une case et d'une position de mur (h,b,g,d), de trouver les coordonnées de la case adjacentes au mur.
    """
    def find_cases_mur(self,x,y,pos):
        if pos == "h":
            return (x-1,y)
        elif pos == "b":
            return (x+1,y)
        elif pos == "g":
            return (x,y-1)
        elif pos == "d":
            return (x,y+1)

    """
    Supprime les fleches des cases.
    """
    def reset_arrows(self):
        for a in self.arrows:
            self.delete(a)
        self.arrows = []

    """
    Supprime les textes des cases.
    """
    def reset_texts(self):
        for t in self.texts:
            self.delete(t)
        self.texts = []

    """
    Permet d'effacer les textes et fleches des cases.
    """
    def reset_res(self):
        self.reset_arrows()
        self.reset_texts()

    """
    Ajout d'un texte (value) dans la case de coordonnées x,y.
    Elle est utilisée pour l'affichage des probabilités de réussite.
    """
    def ajout_text(self,x,y,value):
        unit =self.unit
        dx = unit*x + (unit/2.0)
        dy = unit*y + (unit/2.0)
        text = self.create_text(dy,dx,text=value,font="bold",fill="gray10")
        self.texts.append(text)

    """
    Cette fonction permet l'affichage d'une fleche de couleur "couleur" entre deux cases donnée par les coordonnées (x0,y0) et (x1,y1)
    """
    def create_arrow(self,x0,y0,x1,y1,couleur):
        unit =self.unit
        #centre : taille carré*x + taille carré/2
        dx0 = unit*x0 + (unit/2.0)
        dy0 = unit*y0 + (unit/2.0)
        dx1 = unit*x1 + (unit/2.0)
        dy1 = unit*y1 + (unit/2.0)

        arrow = self.create_line(dy0,dx0,dy1,dx1,fill = couleur, arrow="last",width = 4)
        self.arrows.append(arrow)

    """
    La fonction permet de mettre à jour des elements graphique.
    Le parametre params est compose de coordonnes (x:ligne,y:colonne), de arg (une couleur) et d'un type ("couleur" ou "mur").
    Si le type est "couleur" alors il s'agit d'un changement de couleur d'une case. La nouvelle couleur sera donnée par le parametre arg.
    Si le type est mur, il s'agit alors de la création d'un mur de couleur arg.
    """
    def maj(self,params):
        x,y = params["x"],params["y"]
        arg = params["arg"]
        Type = params["Type"]

        if Type=="couleur":
            self.change_couleur_case(x,y,arg)
        elif Type=="mur":
            self.create_mur(x,y,arg)

    """
    La fonction permet de dessiner les murs qui font partis de la strategie du joueur min.
    Le parametre info est au format (colonnes,ligne,position) une position est du format "h","b","g" ou "d.
    Chaque mur ainsi créé est enregistrer dans un dictionnaire et est aussi rendu cliquable pour permettre de simuler le sous-jeu correspondant.
    """
    def draw_mur_min(self,info):
        unit = self.unit
        x=info[0]
        y=info[1]

        if info[3] == "g":
            ob = self.create_line(y*unit,x*unit,y*unit,(x+1)*unit,fill="Blue",width=6)
            self.mur_min[ob]=(x,y,"g")
            self.tag_bind(ob,'<ButtonPress-1>', self.onObjectClick)
        elif info[3] == "d":
            ob = self.create_line((y+1)*unit,x*unit,(y+1)*unit,(x+1)*unit,fill="Blue",width=6)
            self.mur_min[ob]=(x,y,"d")
            self.tag_bind(ob,'<ButtonPress-1>', self.onObjectClick)
        elif info[3] == "h":
            ob = self.create_line(y*unit,x*unit,(y+1)*unit,x*unit,fill="Blue",width=6)
            self.mur_min[ob]=(x,y,"h")
            self.tag_bind(ob,'<ButtonPress-1>', self.onObjectClick)
        elif info[3] == "b":
            ob = self.create_line(y*unit,(x+1)*unit,(y+1)*unit,(x+1)*unit,fill="Blue",width=6)
            self.mur_min[ob]=(x,y,"b")
            self.tag_bind(ob,'<ButtonPress-1>', self.onObjectClick)

    """
    La fonction permet de supprimer les murs de la strategie min de l'affichage.
    """
    def rm_mur_min(self):
        #Supprime les murs min de l'affichage
        for mur in self.mur_min:
            self.delete(mur)
        self.mur_min={}

    def rm_pion(self):
        self.delete(self.pion)

    def dessine_pion(self,coord_pion):
        self.rm_pion()
        x = coord_pion[0]
        y = coord_pion[1]
        unit = self.unit
        self.pion=self.create_oval(y*unit+5,x*unit+5,(y+1)*unit-5,(x+1)*unit-5,fill="pink")
