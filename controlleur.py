#coding: utf-8

import sys
from vue import *
from modele import *

class Controlleur():
    def __init__(self,vue,modele):
        self.vue=vue
        self.modele=modele

    def button(self,boutton):
        if boutton == "simul":
            self.modele.simulation()

    def click_mur_min(self,info):
        self.modele.init_jeux_terminaux(info,True)
        
if __name__=='__main__':
    if len(sys.argv)>1:
        terrain = Terrain(sys.argv[1])
        terrain.mainloop()
    else:
        print "error pas de fichier regle"
