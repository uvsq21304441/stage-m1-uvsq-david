#coding: utf-8
class Strategie_min():
    """
    La strategie initiale du joueur min est de ne pas bloquer 
    le joueur max quoiqu'il arrive.
    """
    def __init__(self,observateur):
        self.observateur=observateur
        self.nb_murs=0
        self.action={}

    """
    Initialise la strategie.
    La strategie de base est de ne rien bloquer.
    0 = pas de blocage
    1 = blocage
    """
    def init_strat(self,list_sommets):
        for k in list_sommets:
            if k[2]=="min":
                self.action[k]=0
                
    """
    Permet d'appeler la creation des murs que le joueur min pose à l'interface graphique
    """
    def affiche_murs(self):
        for k,v in self.action.items():
            if v==1:
		print k
                self.observateur.draw_mur_min(k)
