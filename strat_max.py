#coding: utf-8
import collections

class Strategie_max():
    def __init__(self,observateur):
        #dictionnaires comprennants les déplacements lié à la stratégie et la valeur associées à chaque case
        self.deplacement={}
        self.valeur={}
        self.observateur=observateur

    #Return True si les deux cases ont un mur les separants
    def is_wall(self,x,y,x2,y2,murs):
        if ((x,y),(x2,y2)) in murs or ((x2,y2),(x,y)) in murs:
            return True
        return False

    #Definit les deplacements selon un parcours en largeur
    def strategie_parcours_largeur(self,nbc,nbl,morts,murs,butx,buty):
        d={}
        for x in range(nbl):
            for y in range(nbc):
                if(x,y) not in morts:
                    d[x,y]=0
        
        f=collections.deque()
        f.append((butx,buty))
        
        while(len(f)>0):
            s=f.popleft()
            d[s]=1

            #h
            if (s[0]-1,s[1]) in d:
                if d[s[0]-1,s[1]]==0 and d[s[0]-1,s[1]] not in morts and self.is_wall(s[0]-1,s[1],s[0],s[1],murs)==False:
                    self.deplacement[s[0]-1,s[1]]="b"
                    f.append((s[0]-1,s[1]))
                    d[s[0]-1,s[1]]=1
                    
            #b
            if (s[0]+1,s[1]) in d:
                if d[s[0]+1,s[1]]==0 and d[s[0]+1,s[1]] not in morts and self.is_wall(s[0]+1,s[1],s[0],s[1],murs)==False:
                    self.deplacement[s[0]+1,s[1]]="h"
                    f.append((s[0]+1,s[1]))
                    d[s[0]+1,s[1]]=1
            #g      
            if (s[0],s[1]-1) in d:
                if d[s[0],s[1]-1]==0 and d[s[0],s[1]-1] not in morts and self.is_wall(s[0],s[1]-1,s[0],s[1],murs)==False:
                    self.deplacement[s[0],s[1]-1]="d"
                    f.append((s[0],s[1]-1))
                    d[s[0],s[1]-1]=1
            #d
            if (s[0],s[1]+1) in d:
                if d[s[0],s[1]+1]==0 and d[s[0],s[1]+1] not in morts and self.is_wall(s[0],s[1]+1,s[0],s[1],murs)==False:
                    self.deplacement[s[0],s[1]+1]="g"
                    f.append((s[0],s[1]+1))
                    d[s[0],s[1]+1]=1
        #self.affiche_arrow()

    """
    Permet la creation des fleches, selon la strategie, dans l'interface graphique.
    """
    def affiche_arrow(self):
        for elt in self.deplacement:
            self.add_arrow(elt,self.deplacement[elt],"red")
    """
    Notifie à la vue la création des fleches.
    """ 
    def add_arrow(self,s1,s2,couleur):
        if s2=="h":
            s2x,s2y=s1[0]-1,s1[1]
        if s2=="b":
            s2x,s2y=s1[0]+1,s1[1]
        if s2=="g":
            s2x,s2y=s1[0],s1[1]-1
        if s2=="d":
            s2x,s2y=s1[0],s1[1]+1
        return self.observateur.create_arrow(s1[0],s1[1],s2x,s2y,couleur)

    """
    Permet l'affichage des valeurs de réussite de chaque case dans l'inteface graphique.
    """
    def affiche_res(self):
        #print self.valeur
        for k,v in self.valeur.items():
            if k[2]=="max" or k[2]=="but":
                 self.add_text(k[0],k[1],round(v*100,3))

    """
    Notifie à la vue l'affichage des valeurs
    """
    def add_text(self,x,y,val):
        return self.observateur.ajout_text(x,y,val)
